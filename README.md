![The Midnight](https://gitlab.com/Cryptic-Mushroom/The-Midnight-Archived/-/raw/1.15.2/front-page-poster.jpg)

# The Midnight

*The Midnight* is a mod being developed by Cryptic Mushroom that adds a new, mysterious and scary dimension to explore. It is a dimension of darkness; the only light comes from crystals and the various life of the dimension. You may travel willingly to this dimension, though you may unwillingly encounter a rifter and be dragged into this strange and hostile world...  

This is the archived source code repository for the Midnight mod, originally made for SpookyJam 2018.

## About this archive

The Midnight for Minecraft 1.12.2, 1.14.4, and 1.15.2 are all open-source under the GPLv3 license, and will continue to remain that way. You may use the code from this repository for whatever you might need, as long as it follows the license.

A new version of the Midnight, for whatever the latest version of Minecraft is, is currently being worked on by a new team, informally known as the rewrite, that **will not be open-source.** [For more information, feel free to visit our Discord server.](https://discord.gg/h7u4Tc7)

## How to get to The Midnight

### 1.15.2

To get to the dimension, you will need to find a rift. These will spawn in red clumps of matter. Though before you go, make sure to be prepared!

Locate a rift structure: `/locate midnight:entrance_rift`  
Teleport directly into The Midnight: `/execute in midnight:midnight run tp @s ~ ~ ~`

### 1.12.2 - 1.14.4

To get to the dimension, you will need to find a rift. These will spawn randomly during the night both above ground and in caves.

Summon a Rift entity: `/summon midnight:rift ~ ~ ~`

## Links of Interest

- [Website](https://crypticmushroom.com)
- [Discord Server](https://discord.gg/h7u4Tc7)
- [GitLab Issue Tracker](https://gitlab.com/Cryptic-Mushroom/The-Midnight/issues)
- [Pre-Rewrite Source Archive](https://gitlab.com/Cryptic-Mushroom/The-Midnight-Archived)

## The Midnight Team (Pre-Rewrite)

### Developers

[Cipher Zero X](https://github.com/cipherzerox) - Project Lead, Mastermind, Modeler (1.12.2 - 1.15.2)  
[Shadew](https://github.com/FoxShadew) - Programmer (1.15.2)  
[Jonathing](https://github.com/Jonathing) - Quality Assurance, Community Manager, Programmer (1.15.2)  
[Lachney](https://xjon.me) - Lead Sound Designer, Lead Musician (1.12.2 - 1.15.2)  
[bagu_chan](https://github.com/pentantan) - Programmer (1.15.2)  
[gegy1000](https://github.com/gegy1000) - Programmer (1.12.2 - 1.14.4)  
[Martacus](https://github.com/Martacus) - Programmer (1.12.2 - 1.14.4)  
[arthurbambous](https://github.com/arthurbambou) - Programmer (1.12.2 - 1.14.4)  
[Corail31](https://github.com/Corail31) - Programmer (1.12.2 - 1.14.4)  
[Endergized](https://github.com/Endergy) - Texturer (1.12.2 - 1.14.4)  
[Five (Paradiscal)](https://github.com/fivelol) - Texturer (1.12.2 - 1.14.4)  
[MCVinnyq](https://github.com/MCVinnyq) - Texturer, Modeler (1.12.2 - 1.14.4)

### Contributors

Contributors: [ZombieEnderman5](https://github.com/ZombieEnderman5), [veesus mikhel heir](https://minecraft.curseforge.com/members/veesusmikelheir), [Terenx](https://github.com/Terenx), [KingPhygieBoo](https://gitlab.com/KingPhygieBoo)